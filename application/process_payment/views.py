from django.shortcuts import render
from rest_framework import viewsets, status
from .serializers import PaymentDetailsSerializer
from .models import PaymentDetails
from rest_framework.response import Response
from .my_validation import Validation
from .process_pay import ProcessPay
import logging

logger = logging.getLogger(__name__)
# Create your views here.


class PaymentDetailsViewSet(viewsets.ModelViewSet):
    queryset = PaymentDetails.objects.all().order_by('card_holder')
    serializer_class = PaymentDetailsSerializer

    def create(self, request):
        serializer = PaymentDetailsSerializer(data=request.data)
        validation = Validation(request.data['credit_card_number'], request.data['card_holder'],
                                request.data['expiration_date'], request.data['amount'], request.data['security_code'])
        if validation.validate_data():
            response = ProcessPay(request.data['credit_card_number'], request.data['card_holder'],
                                  request.data['expiration_date'], request.data['amount'], request.data['security_code'])
            if response.pay():
                if serializer.is_valid():
                    # if we want to save the payment data, we will uncomment the next line
                    # serializer.save()
                    return Response("OK", status=status.HTTP_200_OK)
            else:
                return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)
        return Response("Internal server error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
