from rest_framework import serializers
from .models import PaymentDetails


class PaymentDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PaymentDetails
        fields = ('credit_card_number', 'card_holder', 'expiration_date', 'security_code', 'amount')
