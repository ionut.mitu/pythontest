# we need to import the external payment gateway

LIMIT_FOR_CHEAP_PAYMENT = 20
INFERIOR_LIMIT_FOR_EXPENSIVE_PAYMENT = 21
SUPERIOR_LIMIT_FOR_EXPENSIVE_PAYMENT = 500


class ProcessPay(object):

    def __init__(self, credit_card_number, card_holder, expiration_date, amount, security_code=""):
        self.credit_card_number = credit_card_number
        self.card_holder = card_holder
        self.expiration_date = expiration_date
        self.amount = float(amount)
        self.security_code = security_code

    def pay(self):
        if LIMIT_FOR_CHEAP_PAYMENT >= self.amount:
            # we will use CheapPaymentGateway
            processed = True  # here should be the call to CheapPaymentGateway
            if processed:
                return True
            else:
                return False
        elif INFERIOR_LIMIT_FOR_EXPENSIVE_PAYMENT <= self.amount <= SUPERIOR_LIMIT_FOR_EXPENSIVE_PAYMENT:
            expensive_avaible = True
            if expensive_avaible:
                # we will use ExpensivePaymentHateway if avaible
                processed = True  # here should be the call to  ExpensivePaymentHateway
            else:
                # we will use CheapPaymentGateway
                processed = True

            if processed:
                return True
            else:
                return False
        elif self.amount > SUPERIOR_LIMIT_FOR_EXPENSIVE_PAYMENT:
            # we will use PremiumPaymentGateway
            processed = False
            count = 0
            while count < 3 and not processed:
                # we will call PremiumPaymentGateway on next line
                processed = True   # here should be the call to  PremiumPaymentGateway
                count += 1

            if processed:
                return True
            else:
                return False
