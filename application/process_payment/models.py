from django.db import models

# Create your models here.


class PaymentDetails(models.Model):
    credit_card_number = models.CharField(max_length=16)
    card_holder = models.CharField(max_length=60)
    expiration_date = models.DateTimeField()
    security_code = models.CharField(max_length=3, default="")
    amount = models.DecimalField(max_digits=19, decimal_places=10)

    def __str__(self):
        return self.credit_card_number + self.card_holder + str(self.expiration_date) \
               + self.security_code + str(self.amount)
