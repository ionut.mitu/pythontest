from datetime import date
import datetime


class Validation(object):

    def __init__(self, credit_card_number, card_holder, expiration_date, amount, security_code=""):
        self.credit_card_number = credit_card_number
        self.card_holder = card_holder
        self.expiration_date = datetime.datetime.strptime(expiration_date, "%Y-%m-%dT%H:%M:%SZ")
        self.amount = amount
        self.security_code = security_code

    def _validate_credit_card_number(self):
        if 16 == len(self.credit_card_number):
            return True
        else:
            return False

    def _validate_card_holder(self):
        if 3 <= len(self.card_holder):
            return True
        else:
            return False

    def _validate_expiration_date(self):
        if self.expiration_date > datetime.datetime.now():
            return True
        else:
            return False

    def _validate_amount(self):
        if float(self.amount) > 0.1:
            return True
        else:
            return False

    def _validate_security_code(self):
        if "" == self.security_code:
            return True
        elif 3 == len(self.security_code):
            return True
        else:
            return False

    def validate_data(self):
        if self._validate_expiration_date():
            if self._validate_amount():
                if self._validate_credit_card_number():
                    if self._validate_card_holder():
                        if self._validate_security_code():
                            return True

        return False
